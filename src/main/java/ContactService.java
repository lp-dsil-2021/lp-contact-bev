
public class ContactService implements  IContactService{

    public IContactDao contactDao = new ContactDao();
    

    public Contact creerContact(String nom, String prenom) throws ContactException {
        if (nom == null || nom.trim().length() < 3 || nom.trim().length() > 40 || prenom == null || prenom.trim().length() < 3 || prenom.trim().length() > 40) {
            throw new ContactException();
        }
        if(contactDao.isContactExist(nom, prenom)){
            throw new ContactException();
        }
        Contact contact = new Contact();
        contact.setName(nom);
        contact.setPrenom(prenom);
        contactDao.add(contact);
        return contact;
        //contactDao.isContactExist(nom){
       // throw new ContactException();
    //}

    }
}
