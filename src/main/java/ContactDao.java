import java.util.ArrayList;
import java.util.List;

public class ContactDao implements IContactDao {
    private List<Contact> contacts = new ArrayList<>();

    public void add(Contact contact){
        contacts.add(contact);
    }


    public boolean isContactExist(String nom, String prenom) {
        for(Contact contact : contacts) {
            if(nom.equalsIgnoreCase(contact.getName()) || prenom.equalsIgnoreCase(contact.getPrenom()) ){
                return true;
            }
        }
        return false;
    }


}

