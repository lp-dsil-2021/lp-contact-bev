public interface IContactService {
    /**
     * Méthode qui permet de créer un contact
     * @param nom du contact a créer
     * @return Contact crée
     * @throws ContactException lorsque le contact ne peut-être crée (Nom invalide ou Nom unique)
     */

    Contact creerContact(String nom, String prenom) throws ContactException;
}