import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContactServiceMockTest {



    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Captor
    private ArgumentCaptor<Contact> contactCaptor;

    @Test
    public void shouldFailOnDuplicateEntry() throws ContactException {
        when(contactDao.isContactExist("Rembelet", "Thierry")).thenReturn(true);
        //Test
        Assertions.assertThrows(ContactException.class, ()->contactService.creerContact("Rembelet", "Thierry"));
    }



    @Test
    public void shouldPass() throws ContactException {
        //Déf du Mock
        when(contactDao.isContactExist("Thierry", "Pierre")).thenReturn(false);
        //Test
        contactService.creerContact("Thierry", "Pierre");
        // Capture
        Mockito.verify(contactDao).add(contactCaptor.capture());
        //Je souhaite voir le contenue de l'argument
        Contact contact = contactCaptor.getValue();
        Assertions.assertEquals("Thierry", contact.getName());
    }
}
