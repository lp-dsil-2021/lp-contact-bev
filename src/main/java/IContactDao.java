public interface IContactDao {

     void add(Contact contact);
     boolean isContactExist(String nom, String prenom);
}

