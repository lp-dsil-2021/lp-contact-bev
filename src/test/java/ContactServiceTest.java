
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ContactServiceTest{
    IContactService service = new ContactService();

    @Test
    public void testContactNomNull() throws ContactException {
        Assertions.assertThrows(ContactException.class, () ->
            service.creerContact(null, null)
        );
    }
    @Test
    public void testCreerContactNomTropCourt() throws ContactException {
        Assertions.assertThrows(ContactException.class, () ->
        service.creerContact("az", "azer")
        );
    }
    @Test
    public void testCreerContactNomTropLong() throws ContactException {
        Assertions.assertThrows(ContactException.class, () ->
        service.creerContact("azerttyuuioopqsdfgghhkkldkgrytrytryyryuyturyu", "ffffffffffff"));
    }

    @Test
    public void testCreerContactOk() throws ContactException {
        Contact contactCree = service.creerContact("Rembelet", "Francis");
        Assertions.assertNotNull(contactCree);
        Assertions.assertEquals("Rembelet", contactCree.getName());
    }



}